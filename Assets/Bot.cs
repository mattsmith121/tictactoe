﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bot : MonoBehaviour
{
    [SerializeField] float botDelay;
    [SerializeField] GameManager gameManager;
    [SerializeField] bool O;
    int[,] values;
    bool botTurn;

    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    // Update is called once per frame
    void Update()
    {
        if (botTurn) {
            int x, y;
            // Can we win?
            if (CheckWinCon(out x, out y, false)) {}
            // Can opponent win?
            else if (CheckWinCon(out x, out y, true)) {}
            // Best position
            else {
                BestPosition(out x, out y);
            }

            // Tell GameManager our choice
            gameManager.BoardButtonClicked(x, y, O);
            // Update Values
            UpdateValues(x, y, false);
            botTurn = false;
        }
    }

    bool CheckWinCon(out int x, out int y, bool opp) {
        // Check rows
        for (int i = 0; i < 3; i++) {
            if (CheckRow(i, out x, out y, opp)) {
                return true;
            }
        }
        // Check columns
        for (int i = 0; i < 3; i++) {
            if (CheckColumn(i, out x, out y, opp)) {
                return true;
            }
        }
        // Check diagonals
        if (CheckDiagonals(out x, out y, opp)) {
            return true;
        }
        x = 0; y = 0;
        return false;
    }

    void BestPosition(out int x, out int y) {
        int max = -9999;
        int total = 0;
        x = 0; y = 0;
        // Determine how many max spots there are
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (values[i,j] > max) {
                    max = values[i, j];
                    x = i;
                    y = j;
                    total = 1;
                } else if (values[i,j] == max) {
                    total++;
                }
            }
        }

        // If ther are multiple max spots, then pick a lesser spot
        // otherwise just use the max spot
        if (total == OpenSpots()) {
            // All open spots are max, use priority
            // 1. Middle
            // 2. Corners
            // 3. Sides
            if (values[1, 1] == max && !Taken(1, 1)) {
                x = 1;
                y = 1;
            }
            else if (values[0, 0] == max && !Taken(0, 0)) {
                x = 0; y = 0;
            }
            else if (values[0, 2] == max && !Taken(0, 2)) {
                x = 0; y = 2;
            }
            else if (values[2, 0] == max && !Taken(2, 0)) {
                x = 2; y = 0;
            }
            else if (values[2, 2] == max && !Taken(2, 2)) {
                x = 2; y = 2;
            }
            else if (values[0, 1] == max && !Taken(0, 1)) {
                x = 0; y = 1;
            }
            else if (values[1, 0] == max && !Taken(1, 0)) {
                x = 1; y = 0;
            }
            else if (values[1, 2] == max && !Taken(1, 2)) {
                x = 1; y = 2;
            }
            else if (values[2, 1] == max && !Taken(2, 1)) {
                x = 2; y = 1;
            }
            else {
                Debug.LogError("Bot can't decide what move to make");
            }
        } else if (total > 1) {
            // Use priority to determine which lesser to use
            // 1. Middle
            // 2. Corners
            // 3. Sides
            if (values[1,1] < max && !Taken(1, 1)) {
                x = 1;
                y = 1;
            } else if (values[0,0] < max && !Taken(0, 0)) {
                x = 0; y = 0;
            } else if (values [0, 2] < max && !Taken(0, 2)) {
                x = 0; y = 2;
            } else if (values[2, 0] < max && !Taken(2, 0)) {
                x = 2; y = 0;
            } else if (values[2, 2] < max && !Taken(2, 2)) {
                x = 2; y = 2;
            } else if (values[0, 1] < max && !Taken(0, 1)) {
                x = 0; y = 1;
            } else if (values[1, 0] < max && !Taken(1, 0)) {
                x = 1; y = 0;
            } else if (values[1, 2] < max && !Taken(1, 2)) {
                x = 1; y = 2;
            } else if (values[2, 1] < max && !Taken(2, 1)) {
                x = 2; y = 1;
            } else {
                Debug.LogError("Bot can't decide what move to make");
            }
        }
    }

    int OpenSpots() {
        int total = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!Taken(i, j)) {
                    total++;
                }
            }
        }
        return total;
    }

    bool Taken(int x, int y) {
        return values[x, y] == -1 ||
            values[x, y] == 0;
    }

    bool CheckRow(int r, out int x, out int y, bool opp) {
        int v = opp ? -1 : 0;
        int total = 0;
        x = 0; y = 0;
        if (CheckRowOpponent(r, opp)) {
            return false;
        }
        if (values[r, 0] == v) total++;
        if (values[r, 1] == v) total++;
        if (values[r, 2] == v) total++;
        if (total >= 2) {
            x = r;
            y = values[r, 0] != v ? 0 :
                values[r, 1] != v ? 1 : 2;
            return true;
        }
        return false;
    }

    bool CheckRowOpponent(int r, bool opp) {
        int v = opp ? 0 : -1;
        return values[r, 0] == v ||
            values[r, 1] == v ||
            values[r, 2] == v;
    }

    bool CheckColumn(int c, out int x, out int y, bool opp) {
        int v = opp ? -1 : 0;
        int total = 0;
        x = 0; y = 0;
        if (CheckColumnOpponent(c, opp)) {
            return false;
        }
        if (values[0, c] == v) total++;
        if (values[1, c] == v) total++;
        if (values[2, c] == v) total++;
        if (total >= 2) {
            y = c;
            x = values[0, c] != v ? 0 :
                values[1, c] != v ? 1 : 2;
            return true;
        }
        return false;
    }

    bool CheckColumnOpponent(int c, bool opp) {
        int v = opp ? 0 : -1;
        return values[0, c] == v ||
            values[1, c] == v ||
            values[2, c] == v;
    }
        

    bool CheckDiagonals(out int x, out int y, bool opp) {
        int v = opp ? -1 : 0;
        int q = opp ? 0 : -1;
        x = 0; y = 0;
        // Check if opponent has cut off both diagonals
        int total = 0;
        // Check forward
        if (values[0, 0] != q &&
            values[1, 1] != q &&
            values[2, 2] != q) {
            if (values[0, 0] == v) {
                total++;
            }
            else {
                x = 0; y = 0;
            }
            if (values[1, 1] == v) {
                total++;
            }
            else {
                x = 1; y = 1;
            }
            if (values[2, 2] == v) {
                total++;
            }
            else {
                x = 2; y = 2;
            }
            if (total >= 2) {
                return true;
            }
        }
        
        // Check backward
        total = 0;
        if (values[0, 2] != q &&
            values[1, 1] != q &&
            values[2, 0] != q) {
            if (values[0, 2] == v) {
                total++;
            }
            else {
                x = 0; y = 2;
            }
            if (values[1, 1] == v) {
                total++;
            }
            else {
                x = 1; y = 1;
            }
            if (values[2, 0] == v) {
                total++;
            }
            else {
                x = 2; y = 0;
            }
            if (total >= 2) {
                return true;
            }
        }
        return false;
    }

    void SetValues() {
        values = new int[,] { { 1, 1, 1 },
                              { 1, 1, 1 },
                              { 1, 1, 1 }};
    }

    /// <summary>
    /// Update the values table based on player move.
    /// </summary>
    /// <param name="x">X coordinate of move</param>
    /// <param name="y">Y coordinate of move</param>
    /// <param name="opp">Was it oppoenent's move?</param>
    public void UpdateValues(int x, int y, bool opp) {
        values[x, y] = opp ? -1 : 0;
        if (opp) {
            ModifyValues(x, y);
        }
    }

    void ModifyValues(int x, int y) {
        ModifyRow(x);
        ModifyColumn(y);
        //ModifyDiagonals(x, y);
    }

    void ModifyRow(int r) {
        Modify(r, 0);
        Modify(r, 1);
        Modify(r, 2);
    }

    void ModifyColumn(int c) {
        Modify(0, c);
        Modify(1, c);
        Modify(2, c);
    }

    void ModifyDiagonals(int x, int y) {
        if (x == 0 && y == 0) {
            Modify(1, 1);
            Modify(2, 2);
        }
        else if (x == 1 && y == 1) {
            Modify(0, 0);
            Modify(2, 2);
            Modify(0, 2);
            Modify(2, 0);
        }
        else if (x == 2 && y == 2) {
            Modify(0, 0);
            Modify(1, 1);
        }
        else if (x == 0 && y == 2) {
            Modify(0, 0);
            Modify(2, 0);
        }
        else if (x == 2 && y == 0) {
            Modify(0, 0);
            Modify(0, 2);
        }
    }

    void Modify(int x, int y) {
        if (values[x, y] != -1 && values[x, y] != 0) {
            values[x, y]++;
        }
    }

    public void BotTurn() {
        StartCoroutine(WaitForTurn());
    }

    IEnumerator WaitForTurn() {
        yield return new WaitForSeconds(botDelay);
        botTurn = true;
    }

    public void Reset() {
        SetValues();
    }
}
