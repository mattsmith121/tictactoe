﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardButton : MonoBehaviour
{
    [SerializeField] int coordX;
    [SerializeField] int coordY;
    [SerializeField] GameManager gameManager;
    bool clicked;

    void Start() {
        clicked = false;
    }

    public void HandleClick() {
        if (clicked == true) {
            return;
        }

        gameManager.BoardButtonClicked(coordX, coordY, false);
    }
}
