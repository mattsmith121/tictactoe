﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] Bot bot1;
    [SerializeField] Bot bot2;
    [SerializeField] Button[] boardButtons;
    [SerializeField] Text headerText;
    [SerializeField] GameObject endGameButtons;
    [SerializeField] GameObject replayButtons;

    int[,] board;
    int curMove;
    bool playerTurn;
    bool gameOver;
    bool playerGame;

    struct Move
    {
        public int x;
        public int y;
        public bool O;

        public Move(int x, int y, bool O) {
            this.x = x;
            this.y = y;
            this.O = O;
        }
    }

    List<Move> replay;

    void Start() {
        replay = new List<Move>();
        gameOver = true;
    }

    public void BotTurn(int i) {
        if (i == 1) {
            bot1.BotTurn();
        } else {
            bot2.BotTurn();
        }
    }

    public void BoardButtonClicked(int x, int y, bool O) {
        if (playerGame) {
            PlayerBoardUpdate(x, y, O);
        } else {
            BotBoardUpdate(x, y, O);
        }
    }

    void BotBoardUpdate(int x, int y, bool O) {
        if (gameOver) {
            return;
        }

        replay.Add(new Move(x, y, O));
        board[x, y] = O ? 0 : -1;
        Text t = boardButtons[y + 3 * x].GetComponentInChildren<Text>();
        t.text = O ? "O" : "X";

        // Check if someone won
        if (CheckWin(true)) {
            // p1 win
            headerText.text = "Bot 1 Wins";
            gameOver = true;
            return;
        }
        else if (CheckWin(false)) {
            // p2 win
            headerText.text = "Bot 2 Wins";
            gameOver = true;
            return;
        }
        else if (CheckTie()) {
            // Tie game
            headerText.text = "Cat's Game";
            gameOver = true;
            return;
        }

        bot1.UpdateValues(x, y, !O);
        bot2.UpdateValues(x, y, O);
        if (!O) {
            headerText.text = "Bot 1 Turn";
            bot1.BotTurn();
        } else {
            headerText.text = "Bot 2 Turn";
            bot2.BotTurn();
        }
    }

    void PlayerBoardUpdate(int x, int y, bool O) {
        if (gameOver) {
            return;
        }

        if (O) {
            playerTurn = true;
            headerText.text = "Player Turn";
        }
        if (!O && !playerTurn) {
            return;
        }


        // Check if square already taken
        if (board[x, y] != 1) {
            return;
        }

        replay.Add(new Move(x, y, O));
        board[x, y] = O ? 0 : -1;
        Text t = boardButtons[y + 3 * x].GetComponentInChildren<Text>();
        t.text = O ? "O" : "X";

        // Check if someone won
        if (CheckWin(true)) {
            // p1 win
            headerText.text = "Player Wins";
            gameOver = true;
            return;
        }
        else if (CheckWin(false)) {
            // p2 win
            headerText.text = "Bot Wins";
            gameOver = true;
            return;
        }
        else if (CheckTie()) {
            // Tie game
            headerText.text = "Cat's Game";
            gameOver = true;
            return;
        }

        bot1.UpdateValues(x, y, !O);
        if (!O) {
            headerText.text = "Bot Turn";
            playerTurn = false;
            bot1.BotTurn();
        }
    }

    bool CheckWin(bool p2) {
        // Check rows
        for (int i = 0; i < 3; i++) {
            if (CheckRow(i, p2)) {
                return true;
            }
        }
        // Check columns
        for (int i = 0; i < 3; i++) {
            if (CheckColumn(i, p2)) {
                return true;
            }
        }
        // Check diagonals
        if (CheckDiagonals(p2)) {
            return true;
        }

        return false;
    }

    bool CheckRow(int r, bool p2) {
        int v = p2 ? 0 : -1;
        if (board[r, 0] == v &&
            board[r, 1] == v &&
            board[r, 2] == v) {
            return true;
        }
        return false;
    }

    bool CheckColumn(int c, bool p2) {
        int v = p2 ? -1 : 0;
        if (board[0, c] == v &&
            board[1, c] == v &&
            board[2, c] == v) {
            return true;
        }
        return false;
    }

    bool CheckDiagonals(bool p2) {
        int v = p2 ? -1 : 0;
        if (board[0,0] == v &&
            board[1,1] == v &&
            board[2,2] == v) {
            return true;
        } else if (board[0,2] == v &&
            board[1,1] == v && 
            board[2,0] == v) {
            return true;
        }
        return false;
    }

    /// <summary>
    /// If the entire board is taken and CheckWin didn't trigger
    /// then it is a tie.
    /// </summary>
    /// <returns>True if game is a tie, false otherwise</returns>
    bool CheckTie() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i,j] == 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public void PvB() {
        playerGame = true;
        Reset();
    }

    public void BvB() {
        playerGame = false;
        Reset();
    }

    /// <summary>
    /// Play again
    /// </summary>
    public void Reset() {
        gameOver = false;
        bot1.Reset();
        bot2.Reset();
        replay.Clear();
        replayButtons.SetActive(false);
        ResetBoard();
        if (Random.Range(0, 2) == 1) {
            if (playerGame) {
                playerTurn = true;
                headerText.text = "Player Turn";
            } else {
                BotTurn(2);
                headerText.text = "Bot 2 Turn";
            }
        }
        else {
            BotTurn(1);
            headerText.text = "Bot 1 Turn";
        }
    }

    public void WatchReplay() {
        curMove = -1;
        ResetBoard();
        replayButtons.SetActive(true);
    }

    public void NextMove() {
        if (curMove == replay.Count) {
            return;
        }
        curMove++;
        int x = replay[curMove].x;
        int y = replay[curMove].y;
        Text t = boardButtons[y + 3 * x].GetComponentInChildren<Text>();
        t.text = replay[curMove].O ? "O" : "X";
    }

    public void PreviousMove() {
        if (curMove == -1) {
            return;
        }
        int x = replay[curMove].x;
        int y = replay[curMove].y;
        Text t = boardButtons[y + 3 * x].GetComponentInChildren<Text>();
        t.text = "";
        curMove--;
    }

    void ResetBoard() {
        foreach (Button b in boardButtons) {
            b.GetComponentInChildren<Text>().text = "";
        }
        board = new int[,] { { 1, 1, 1 },
                             { 1, 1, 1 },
                             { 1, 1, 1 }};
    }
}
